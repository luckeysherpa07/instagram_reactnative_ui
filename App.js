import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import MainScreen from './component/MainScreen';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

export default class App extends React.Component {
  render() {
    return (
      <AppContainer />
    );
  }
}

const MainNavigator = createStackNavigator({
  Main: {
    screen: MainScreen
  }
},
{
  defaultNavigationOptions: {
    header: null
  },
});

const AppContainer = createAppContainer(MainNavigator);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
