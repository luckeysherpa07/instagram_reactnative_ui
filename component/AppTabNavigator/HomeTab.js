import React, { Component } from "react";
import { 
    View,
    Text,
    StyleSheet,
    ScrollView,
    Platform,
    StatusBar,
    Image
} from "react-native";
import { Container, Content, Thumbnail, Header, Left, Body, Right, Icon} from 'native-base';
import axios from 'axios';

import CardComponent from './../CardComponent';

class HomeTab extends Component{
    state = {
      posts: []
    };
    componentDidMount() {
      axios.get("https://jsonplaceholder.typicode.com/photos").then(res => {
        this.setState({
          posts: res.data.slice(0, 10)
        });
      });
    }
    render() {
        const { posts } = this.state;
        const postList = posts.length ? (
            posts.map(post => {
            return (
                    <CardComponent imageSource={post.url} likes="301" imageTitle={post.title} />
                );
            })
        ) : (
            <Text>Nothing to Display</Text>
        );
        return (
            <Container style={styles.container}>
                <Header>
                    <Left><Icon name="camera" style={{paddingLeft: 10}} /></Left>
                    <Body><Text>Instagram</Text></Body>
                    <Right><Icon name="send" style={{paddingRight: 10}} /></Right>
                </Header>
                <Content>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 7}}>
                        <Text>Stories</Text>
                        <Text>Watch All</Text>
                    </View>
                    <View style={{flex: 3}}>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{
                                alignItems: 'center',
                                paddingStart: 5,
                                paddingEnd: 5
                            }}
                        >
                            <Thumbnail 
                                style={{marginHorizontal: 5, borderColor: 'pink', borderWidth: 2}}
                                source={require('./../../assets/Propic.jpg')} 
                            />
                            <Thumbnail 
                                style={{marginHorizontal: 5, borderColor: 'pink', borderWidth: 2}}
                                source={require('./../../assets/Propic.jpg')} 
                            />
                            <Thumbnail 
                                style={{marginHorizontal: 5, borderColor: 'pink', borderWidth: 2}}
                                source={require('./../../assets/Propic.jpg')} 
                            />
                            <Thumbnail 
                                style={{marginHorizontal: 5, borderColor: 'pink', borderWidth: 2}}
                                source={require('./../../assets/Propic.jpg')} 
                            />
                            <Thumbnail 
                                style={{marginHorizontal: 5, borderColor: 'pink', borderWidth: 2}}
                                source={require('./../../assets/Propic.jpg')} 
                            />
                            <Thumbnail 
                                style={{marginHorizontal: 5, borderColor: 'pink', borderWidth: 2}}
                                source={require('./../../assets/Propic.jpg')} 
                            />
                        </ScrollView>
                    </View>
                    {postList}
                </Content>
            </Container>
        )
    }
}
export default HomeTab;

const styles = StyleSheet.create({
    container: {
            flex: 1,
            ...Platform.select({
                android: {
                    marginTop: StatusBar.currentHeight
                }
            })

        }
});