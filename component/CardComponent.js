import React, { Component } from "react";
import { 
    Text,
    StyleSheet,
    Image
} from "react-native";

import { Card, CardItem, Thumbnail, Body, Left, Right, Button, Icon} from 'native-base';

class CardComponent extends Component {
    render(){

        const images = {
            "1": require('./../assets/Propic.jpg'),
            "2": require('./../assets/Propic.jpg'),
            "3": require('./../assets/Propic.jpg'),
        }

        return (     
            <Card>
                <CardItem>
                    <Left>
                        <Thumbnail source={require('./../assets/Propic.jpg')} />
                        <Body>
                            <Text>Luckey </Text>
                            <Text note>Dec 22,2018</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem>
                    <Image 
                        source={{uri: this.props.imageSource}}
                        style={{height: 200, width: null, flex: 1}} 
                    />
                </CardItem>
                <CardItem style={{height: 35}}>
                    <Left>
                        <Button transparent>
                            <Icon name="heart"
                                sytle={{ color: 'block' }}
                            />  
                        </Button>
                        <Button transparent>
                            <Icon name="chatbubbles"
                                sytle={{ color: 'block' }}
                            />  
                        </Button>
                        <Button transparent>
                            <Icon name="send"
                                sytle={{ color: 'block' }}
                            />  
                        </Button>
                    </Left>
                </CardItem>
                <CardItem style={{height: 20}}>
                    <Text>{this.props.likes}</Text>
                </CardItem>
                <CardItem>
                    <Body>
                        <Text style={{fontWeight: '900'}}>Luckey </Text>
                        <Text>
                            {this.props.imageTitle}
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        )
    }
}
export default CardComponent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});